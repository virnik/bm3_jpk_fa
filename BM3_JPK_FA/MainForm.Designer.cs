﻿namespace BM3_JPK_FA
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wczytajBM3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importujKontrahentówToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importujPlikJPKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eksportujPlikJPKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.walidujPlikJPKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.zapiszDaneFirmyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.zakończToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aktywacjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.oProgramieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabJPK = new System.Windows.Forms.TabPage();
            this.gbJPK = new System.Windows.Forms.GroupBox();
            this.tbWartoscWierszyFakturNetto = new System.Windows.Forms.TextBox();
            this.labWierszyFakturNetto = new System.Windows.Forms.Label();
            this.tbLacznaWartoscFakturBrutto = new System.Windows.Forms.TextBox();
            this.labLacznaWartoscFakturBrutto = new System.Windows.Forms.Label();
            this.tbLiczbaWierszyWFakturach = new System.Windows.Forms.TextBox();
            this.labLacznaLiczbaWierszyWFakturach = new System.Windows.Forms.Label();
            this.tbLiczbaFaktur = new System.Windows.Forms.TextBox();
            this.labLiczbafaktur = new System.Windows.Forms.Label();
            this.tbOkresdo = new System.Windows.Forms.TextBox();
            this.labokresDo = new System.Windows.Forms.Label();
            this.tbOkresOd = new System.Windows.Forms.TextBox();
            this.labOkres = new System.Windows.Forms.Label();
            this.cbWaluta = new System.Windows.Forms.ComboBox();
            this.labKodWalutowy = new System.Windows.Forms.Label();
            this.cbUS = new System.Windows.Forms.ComboBox();
            this.labUS = new System.Windows.Forms.Label();
            this.gbDanePodatnika = new System.Windows.Forms.GroupBox();
            this.rbBruttoNetto = new System.Windows.Forms.RadioButton();
            this.rbNettoBrutto = new System.Windows.Forms.RadioButton();
            this.labKierunek = new System.Windows.Forms.Label();
            this.tbKodPocztowy = new System.Windows.Forms.MaskedTextBox();
            this.tbNIP = new System.Windows.Forms.MaskedTextBox();
            this.tbPoczta = new System.Windows.Forms.TextBox();
            this.labPoczta = new System.Windows.Forms.Label();
            this.labKodPocztowy = new System.Windows.Forms.Label();
            this.tbMiejscowosc = new System.Windows.Forms.TextBox();
            this.labMiejscowosc = new System.Windows.Forms.Label();
            this.tbNrLokalu = new System.Windows.Forms.TextBox();
            this.labNrLokalu = new System.Windows.Forms.Label();
            this.tbNrDomu = new System.Windows.Forms.TextBox();
            this.labNrDomu = new System.Windows.Forms.Label();
            this.tbUlica = new System.Windows.Forms.TextBox();
            this.labUlica = new System.Windows.Forms.Label();
            this.tbGmina = new System.Windows.Forms.TextBox();
            this.labGmina = new System.Windows.Forms.Label();
            this.tbPowiat = new System.Windows.Forms.TextBox();
            this.labPowiat = new System.Windows.Forms.Label();
            this.cbWojewodztwo = new System.Windows.Forms.ComboBox();
            this.labWojewodztwo = new System.Windows.Forms.Label();
            this.cbKraj = new System.Windows.Forms.ComboBox();
            this.labKraj = new System.Windows.Forms.Label();
            this.tbPelnaNazwa = new System.Windows.Forms.TextBox();
            this.labPelnaNazwa = new System.Windows.Forms.Label();
            this.tbRegon = new System.Windows.Forms.TextBox();
            this.labNip = new System.Windows.Forms.Label();
            this.labRgon = new System.Windows.Forms.Label();
            this.tabFaktury = new System.Windows.Forms.TabPage();
            this.dgFaktury = new System.Windows.Forms.DataGridView();
            this.tabPozycjeFaktur = new System.Windows.Forms.TabPage();
            this.dgPozycjeFaktur = new System.Windows.Forms.DataGridView();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.operacjaStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.licencjaLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.openDBF = new System.Windows.Forms.OpenFileDialog();
            this.openJPK = new System.Windows.Forms.OpenFileDialog();
            this.saveJPK = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabJPK.SuspendLayout();
            this.gbJPK.SuspendLayout();
            this.gbDanePodatnika.SuspendLayout();
            this.tabFaktury.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFaktury)).BeginInit();
            this.tabPozycjeFaktur.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPozycjeFaktur)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.pomocToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(770, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wczytajBM3ToolStripMenuItem,
            this.importujKontrahentówToolStripMenuItem,
            this.importujPlikJPKToolStripMenuItem,
            this.eksportujPlikJPKToolStripMenuItem,
            this.walidujPlikJPKToolStripMenuItem,
            this.toolStripSeparator1,
            this.zapiszDaneFirmyToolStripMenuItem,
            this.toolStripSeparator3,
            this.zakończToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // wczytajBM3ToolStripMenuItem
            // 
            this.wczytajBM3ToolStripMenuItem.Name = "wczytajBM3ToolStripMenuItem";
            this.wczytajBM3ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.wczytajBM3ToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.wczytajBM3ToolStripMenuItem.Text = "Importuj dane BM3...";
            this.wczytajBM3ToolStripMenuItem.Click += new System.EventHandler(this.wczytajBM3ToolStripMenuItem_Click);
            // 
            // importujKontrahentówToolStripMenuItem
            // 
            this.importujKontrahentówToolStripMenuItem.Name = "importujKontrahentówToolStripMenuItem";
            this.importujKontrahentówToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.importujKontrahentówToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.importujKontrahentówToolStripMenuItem.Text = "Importuj kontrahentów...";
            this.importujKontrahentówToolStripMenuItem.Click += new System.EventHandler(this.importujKontrahentowToolStripMenuItem_Click);
            // 
            // importujPlikJPKToolStripMenuItem
            // 
            this.importujPlikJPKToolStripMenuItem.Name = "importujPlikJPKToolStripMenuItem";
            this.importujPlikJPKToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.importujPlikJPKToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.importujPlikJPKToolStripMenuItem.Text = "Importuj plik JPK...";
            this.importujPlikJPKToolStripMenuItem.Click += new System.EventHandler(this.importujPlikJPKToolStripMenuItem_Click);
            // 
            // eksportujPlikJPKToolStripMenuItem
            // 
            this.eksportujPlikJPKToolStripMenuItem.Enabled = false;
            this.eksportujPlikJPKToolStripMenuItem.Name = "eksportujPlikJPKToolStripMenuItem";
            this.eksportujPlikJPKToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.eksportujPlikJPKToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.eksportujPlikJPKToolStripMenuItem.Text = "Eksportuj plik JPK...";
            this.eksportujPlikJPKToolStripMenuItem.Click += new System.EventHandler(this.eksportujPlikJPKToolStripMenuItem_Click);
            // 
            // walidujPlikJPKToolStripMenuItem
            // 
            this.walidujPlikJPKToolStripMenuItem.Name = "walidujPlikJPKToolStripMenuItem";
            this.walidujPlikJPKToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.walidujPlikJPKToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.walidujPlikJPKToolStripMenuItem.Text = "Waliduj plik JPK...";
            this.walidujPlikJPKToolStripMenuItem.Click += new System.EventHandler(this.walidujPlikJPKToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(222, 6);
            // 
            // zapiszDaneFirmyToolStripMenuItem
            // 
            this.zapiszDaneFirmyToolStripMenuItem.Name = "zapiszDaneFirmyToolStripMenuItem";
            this.zapiszDaneFirmyToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.zapiszDaneFirmyToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.zapiszDaneFirmyToolStripMenuItem.Text = "Zapisz dane firmy";
            this.zapiszDaneFirmyToolStripMenuItem.Click += new System.EventHandler(this.ZapiszDaneFirmyToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(222, 6);
            // 
            // zakończToolStripMenuItem
            // 
            this.zakończToolStripMenuItem.Name = "zakończToolStripMenuItem";
            this.zakończToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.zakończToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.zakończToolStripMenuItem.Text = "Zakończ";
            this.zakończToolStripMenuItem.Click += new System.EventHandler(this.CloseToolStripMenuItem_Click);
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aktywacjaToolStripMenuItem,
            this.toolStripSeparator2,
            this.oProgramieToolStripMenuItem});
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.pomocToolStripMenuItem.Text = "Pomoc";
            // 
            // aktywacjaToolStripMenuItem
            // 
            this.aktywacjaToolStripMenuItem.Name = "aktywacjaToolStripMenuItem";
            this.aktywacjaToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.aktywacjaToolStripMenuItem.Text = "Pomoc";
            this.aktywacjaToolStripMenuItem.Click += new System.EventHandler(this.aktywacjaToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(138, 6);
            // 
            // oProgramieToolStripMenuItem
            // 
            this.oProgramieToolStripMenuItem.Name = "oProgramieToolStripMenuItem";
            this.oProgramieToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.oProgramieToolStripMenuItem.Text = "O programie";
            this.oProgramieToolStripMenuItem.Click += new System.EventHandler(this.oProgramieToolStripMenuItem_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabJPK);
            this.tabControl.Controls.Add(this.tabFaktury);
            this.tabControl.Controls.Add(this.tabPozycjeFaktur);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(770, 292);
            this.tabControl.TabIndex = 3;
            this.tabControl.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControler_Selecting);
            // 
            // tabJPK
            // 
            this.tabJPK.Controls.Add(this.gbJPK);
            this.tabJPK.Controls.Add(this.gbDanePodatnika);
            this.tabJPK.Location = new System.Drawing.Point(4, 22);
            this.tabJPK.Name = "tabJPK";
            this.tabJPK.Padding = new System.Windows.Forms.Padding(3);
            this.tabJPK.Size = new System.Drawing.Size(762, 266);
            this.tabJPK.TabIndex = 0;
            this.tabJPK.Text = "JPK";
            this.tabJPK.UseVisualStyleBackColor = true;
            // 
            // gbJPK
            // 
            this.gbJPK.Controls.Add(this.tbWartoscWierszyFakturNetto);
            this.gbJPK.Controls.Add(this.labWierszyFakturNetto);
            this.gbJPK.Controls.Add(this.tbLacznaWartoscFakturBrutto);
            this.gbJPK.Controls.Add(this.labLacznaWartoscFakturBrutto);
            this.gbJPK.Controls.Add(this.tbLiczbaWierszyWFakturach);
            this.gbJPK.Controls.Add(this.labLacznaLiczbaWierszyWFakturach);
            this.gbJPK.Controls.Add(this.tbLiczbaFaktur);
            this.gbJPK.Controls.Add(this.labLiczbafaktur);
            this.gbJPK.Controls.Add(this.tbOkresdo);
            this.gbJPK.Controls.Add(this.labokresDo);
            this.gbJPK.Controls.Add(this.tbOkresOd);
            this.gbJPK.Controls.Add(this.labOkres);
            this.gbJPK.Controls.Add(this.cbWaluta);
            this.gbJPK.Controls.Add(this.labKodWalutowy);
            this.gbJPK.Controls.Add(this.cbUS);
            this.gbJPK.Controls.Add(this.labUS);
            this.gbJPK.Location = new System.Drawing.Point(454, 6);
            this.gbJPK.Name = "gbJPK";
            this.gbJPK.Size = new System.Drawing.Size(301, 255);
            this.gbJPK.TabIndex = 6;
            this.gbJPK.TabStop = false;
            this.gbJPK.Text = "JPK";
            // 
            // tbWartoscWierszyFakturNetto
            // 
            this.tbWartoscWierszyFakturNetto.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tbWartoscWierszyFakturNetto.Location = new System.Drawing.Point(165, 210);
            this.tbWartoscWierszyFakturNetto.Name = "tbWartoscWierszyFakturNetto";
            this.tbWartoscWierszyFakturNetto.ReadOnly = true;
            this.tbWartoscWierszyFakturNetto.Size = new System.Drawing.Size(99, 20);
            this.tbWartoscWierszyFakturNetto.TabIndex = 38;
            // 
            // labWierszyFakturNetto
            // 
            this.labWierszyFakturNetto.AutoSize = true;
            this.labWierszyFakturNetto.Location = new System.Drawing.Point(13, 213);
            this.labWierszyFakturNetto.Name = "labWierszyFakturNetto";
            this.labWierszyFakturNetto.Size = new System.Drawing.Size(144, 13);
            this.labWierszyFakturNetto.TabIndex = 37;
            this.labWierszyFakturNetto.Text = "Wartość wierszy faktur netto:";
            // 
            // tbLacznaWartoscFakturBrutto
            // 
            this.tbLacznaWartoscFakturBrutto.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tbLacznaWartoscFakturBrutto.Location = new System.Drawing.Point(165, 184);
            this.tbLacznaWartoscFakturBrutto.Name = "tbLacznaWartoscFakturBrutto";
            this.tbLacznaWartoscFakturBrutto.ReadOnly = true;
            this.tbLacznaWartoscFakturBrutto.Size = new System.Drawing.Size(99, 20);
            this.tbLacznaWartoscFakturBrutto.TabIndex = 36;
            // 
            // labLacznaWartoscFakturBrutto
            // 
            this.labLacznaWartoscFakturBrutto.AutoSize = true;
            this.labLacznaWartoscFakturBrutto.Location = new System.Drawing.Point(13, 187);
            this.labLacznaWartoscFakturBrutto.Name = "labLacznaWartoscFakturBrutto";
            this.labLacznaWartoscFakturBrutto.Size = new System.Drawing.Size(146, 13);
            this.labLacznaWartoscFakturBrutto.TabIndex = 35;
            this.labLacznaWartoscFakturBrutto.Text = "Łączna wartość faktur brutto:";
            // 
            // tbLiczbaWierszyWFakturach
            // 
            this.tbLiczbaWierszyWFakturach.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tbLiczbaWierszyWFakturach.Location = new System.Drawing.Point(165, 158);
            this.tbLiczbaWierszyWFakturach.Name = "tbLiczbaWierszyWFakturach";
            this.tbLiczbaWierszyWFakturach.ReadOnly = true;
            this.tbLiczbaWierszyWFakturach.Size = new System.Drawing.Size(99, 20);
            this.tbLiczbaWierszyWFakturach.TabIndex = 34;
            // 
            // labLacznaLiczbaWierszyWFakturach
            // 
            this.labLacznaLiczbaWierszyWFakturach.AutoSize = true;
            this.labLacznaLiczbaWierszyWFakturach.Location = new System.Drawing.Point(13, 161);
            this.labLacznaLiczbaWierszyWFakturach.Name = "labLacznaLiczbaWierszyWFakturach";
            this.labLacznaLiczbaWierszyWFakturach.Size = new System.Drawing.Size(137, 13);
            this.labLacznaLiczbaWierszyWFakturach.TabIndex = 33;
            this.labLacznaLiczbaWierszyWFakturach.Text = "Liczba wierszy w fakturach:";
            // 
            // tbLiczbaFaktur
            // 
            this.tbLiczbaFaktur.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tbLiczbaFaktur.Location = new System.Drawing.Point(108, 132);
            this.tbLiczbaFaktur.Name = "tbLiczbaFaktur";
            this.tbLiczbaFaktur.ReadOnly = true;
            this.tbLiczbaFaktur.Size = new System.Drawing.Size(62, 20);
            this.tbLiczbaFaktur.TabIndex = 29;
            // 
            // labLiczbafaktur
            // 
            this.labLiczbafaktur.AutoSize = true;
            this.labLiczbafaktur.Location = new System.Drawing.Point(13, 135);
            this.labLiczbafaktur.Name = "labLiczbafaktur";
            this.labLiczbafaktur.Size = new System.Drawing.Size(71, 13);
            this.labLiczbafaktur.TabIndex = 28;
            this.labLiczbafaktur.Text = "Liczba faktur:";
            // 
            // tbOkresdo
            // 
            this.tbOkresdo.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tbOkresdo.Location = new System.Drawing.Point(215, 106);
            this.tbOkresdo.Name = "tbOkresdo";
            this.tbOkresdo.ReadOnly = true;
            this.tbOkresdo.Size = new System.Drawing.Size(62, 20);
            this.tbOkresdo.TabIndex = 32;
            // 
            // labokresDo
            // 
            this.labokresDo.AutoSize = true;
            this.labokresDo.Location = new System.Drawing.Point(181, 109);
            this.labokresDo.Name = "labokresDo";
            this.labokresDo.Size = new System.Drawing.Size(22, 13);
            this.labokresDo.TabIndex = 31;
            this.labokresDo.Text = "do:";
            // 
            // tbOkresOd
            // 
            this.tbOkresOd.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tbOkresOd.Location = new System.Drawing.Point(108, 106);
            this.tbOkresOd.Name = "tbOkresOd";
            this.tbOkresOd.ReadOnly = true;
            this.tbOkresOd.Size = new System.Drawing.Size(62, 20);
            this.tbOkresOd.TabIndex = 28;
            // 
            // labOkres
            // 
            this.labOkres.AutoSize = true;
            this.labOkres.Location = new System.Drawing.Point(15, 106);
            this.labOkres.Name = "labOkres";
            this.labOkres.Size = new System.Drawing.Size(87, 13);
            this.labOkres.TabIndex = 30;
            this.labOkres.Text = "JPK za okres od:";
            // 
            // cbWaluta
            // 
            this.cbWaluta.BackColor = System.Drawing.SystemColors.Window;
            this.cbWaluta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWaluta.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbWaluta.FormattingEnabled = true;
            this.cbWaluta.Location = new System.Drawing.Point(144, 75);
            this.cbWaluta.Name = "cbWaluta";
            this.cbWaluta.Size = new System.Drawing.Size(133, 21);
            this.cbWaluta.TabIndex = 28;
            this.cbWaluta.Visible = false;
            // 
            // labKodWalutowy
            // 
            this.labKodWalutowy.AutoSize = true;
            this.labKodWalutowy.Location = new System.Drawing.Point(15, 78);
            this.labKodWalutowy.Name = "labKodWalutowy";
            this.labKodWalutowy.Size = new System.Drawing.Size(127, 13);
            this.labKodWalutowy.TabIndex = 29;
            this.labKodWalutowy.Text = "*Domyślny kod walutowy:";
            this.labKodWalutowy.Visible = false;
            // 
            // cbUS
            // 
            this.cbUS.BackColor = System.Drawing.SystemColors.Info;
            this.cbUS.DisplayMember = "NazwaUS";
            this.cbUS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUS.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbUS.FormattingEnabled = true;
            this.cbUS.Location = new System.Drawing.Point(16, 45);
            this.cbUS.Name = "cbUS";
            this.cbUS.Size = new System.Drawing.Size(261, 21);
            this.cbUS.TabIndex = 28;
            this.cbUS.ValueMember = "Kod";
            // 
            // labUS
            // 
            this.labUS.AutoSize = true;
            this.labUS.Location = new System.Drawing.Point(12, 29);
            this.labUS.Name = "labUS";
            this.labUS.Size = new System.Drawing.Size(269, 13);
            this.labUS.TabIndex = 27;
            this.labUS.Text = "*Urząd Skarbowy, do którego adresowany jest plik JPK:";
            // 
            // gbDanePodatnika
            // 
            this.gbDanePodatnika.Controls.Add(this.rbBruttoNetto);
            this.gbDanePodatnika.Controls.Add(this.rbNettoBrutto);
            this.gbDanePodatnika.Controls.Add(this.labKierunek);
            this.gbDanePodatnika.Controls.Add(this.tbKodPocztowy);
            this.gbDanePodatnika.Controls.Add(this.tbNIP);
            this.gbDanePodatnika.Controls.Add(this.tbPoczta);
            this.gbDanePodatnika.Controls.Add(this.labPoczta);
            this.gbDanePodatnika.Controls.Add(this.labKodPocztowy);
            this.gbDanePodatnika.Controls.Add(this.tbMiejscowosc);
            this.gbDanePodatnika.Controls.Add(this.labMiejscowosc);
            this.gbDanePodatnika.Controls.Add(this.tbNrLokalu);
            this.gbDanePodatnika.Controls.Add(this.labNrLokalu);
            this.gbDanePodatnika.Controls.Add(this.tbNrDomu);
            this.gbDanePodatnika.Controls.Add(this.labNrDomu);
            this.gbDanePodatnika.Controls.Add(this.tbUlica);
            this.gbDanePodatnika.Controls.Add(this.labUlica);
            this.gbDanePodatnika.Controls.Add(this.tbGmina);
            this.gbDanePodatnika.Controls.Add(this.labGmina);
            this.gbDanePodatnika.Controls.Add(this.tbPowiat);
            this.gbDanePodatnika.Controls.Add(this.labPowiat);
            this.gbDanePodatnika.Controls.Add(this.cbWojewodztwo);
            this.gbDanePodatnika.Controls.Add(this.labWojewodztwo);
            this.gbDanePodatnika.Controls.Add(this.cbKraj);
            this.gbDanePodatnika.Controls.Add(this.labKraj);
            this.gbDanePodatnika.Controls.Add(this.tbPelnaNazwa);
            this.gbDanePodatnika.Controls.Add(this.labPelnaNazwa);
            this.gbDanePodatnika.Controls.Add(this.tbRegon);
            this.gbDanePodatnika.Controls.Add(this.labNip);
            this.gbDanePodatnika.Controls.Add(this.labRgon);
            this.gbDanePodatnika.Location = new System.Drawing.Point(8, 6);
            this.gbDanePodatnika.Name = "gbDanePodatnika";
            this.gbDanePodatnika.Size = new System.Drawing.Size(440, 255);
            this.gbDanePodatnika.TabIndex = 5;
            this.gbDanePodatnika.TabStop = false;
            this.gbDanePodatnika.Text = "Dane podatnika";
            // 
            // rbBruttoNetto
            // 
            this.rbBruttoNetto.AutoSize = true;
            this.rbBruttoNetto.Checked = true;
            this.rbBruttoNetto.Location = new System.Drawing.Point(130, 225);
            this.rbBruttoNetto.Name = "rbBruttoNetto";
            this.rbBruttoNetto.Size = new System.Drawing.Size(91, 17);
            this.rbBruttoNetto.TabIndex = 32;
            this.rbBruttoNetto.TabStop = true;
            this.rbBruttoNetto.Text = "brutto -> netto";
            this.rbBruttoNetto.UseVisualStyleBackColor = true;
            this.rbBruttoNetto.CheckedChanged += new System.EventHandler(this.rbBruttoNetto_CheckedChanged);
            // 
            // rbNettoBrutto
            // 
            this.rbNettoBrutto.AutoSize = true;
            this.rbNettoBrutto.Location = new System.Drawing.Point(230, 225);
            this.rbNettoBrutto.Name = "rbNettoBrutto";
            this.rbNettoBrutto.Size = new System.Drawing.Size(91, 17);
            this.rbNettoBrutto.TabIndex = 31;
            this.rbNettoBrutto.Text = "netto -> brutto";
            this.rbNettoBrutto.UseVisualStyleBackColor = true;
            this.rbNettoBrutto.CheckedChanged += new System.EventHandler(this.rbBruttoNetto_CheckedChanged);
            // 
            // labKierunek
            // 
            this.labKierunek.AutoSize = true;
            this.labKierunek.Location = new System.Drawing.Point(13, 227);
            this.labKierunek.Name = "labKierunek";
            this.labKierunek.Size = new System.Drawing.Size(110, 13);
            this.labKierunek.TabIndex = 30;
            this.labKierunek.Text = "Kierunek przeliczania:";
            // 
            // tbKodPocztowy
            // 
            this.tbKodPocztowy.BackColor = System.Drawing.SystemColors.Info;
            this.tbKodPocztowy.Enabled = false;
            this.tbKodPocztowy.Location = new System.Drawing.Point(303, 197);
            this.tbKodPocztowy.Mask = "00-000";
            this.tbKodPocztowy.Name = "tbKodPocztowy";
            this.tbKodPocztowy.Size = new System.Drawing.Size(52, 20);
            this.tbKodPocztowy.TabIndex = 29;
            // 
            // tbNIP
            // 
            this.tbNIP.BackColor = System.Drawing.SystemColors.Info;
            this.tbNIP.Location = new System.Drawing.Point(92, 64);
            this.tbNIP.Mask = "0000000000";
            this.tbNIP.Name = "tbNIP";
            this.tbNIP.Size = new System.Drawing.Size(121, 20);
            this.tbNIP.TabIndex = 28;
            // 
            // tbPoczta
            // 
            this.tbPoczta.BackColor = System.Drawing.SystemColors.Info;
            this.tbPoczta.Location = new System.Drawing.Point(92, 198);
            this.tbPoczta.Name = "tbPoczta";
            this.tbPoczta.Size = new System.Drawing.Size(121, 20);
            this.tbPoczta.TabIndex = 27;
            // 
            // labPoczta
            // 
            this.labPoczta.AutoSize = true;
            this.labPoczta.Location = new System.Drawing.Point(13, 201);
            this.labPoczta.Name = "labPoczta";
            this.labPoczta.Size = new System.Drawing.Size(47, 13);
            this.labPoczta.TabIndex = 26;
            this.labPoczta.Text = "*Poczta:";
            // 
            // labKodPocztowy
            // 
            this.labKodPocztowy.AutoSize = true;
            this.labKodPocztowy.Location = new System.Drawing.Point(224, 200);
            this.labKodPocztowy.Name = "labKodPocztowy";
            this.labKodPocztowy.Size = new System.Drawing.Size(81, 13);
            this.labKodPocztowy.TabIndex = 24;
            this.labKodPocztowy.Text = "*Kod pocztowy:";
            // 
            // tbMiejscowosc
            // 
            this.tbMiejscowosc.BackColor = System.Drawing.SystemColors.Info;
            this.tbMiejscowosc.Enabled = false;
            this.tbMiejscowosc.Location = new System.Drawing.Point(92, 172);
            this.tbMiejscowosc.Name = "tbMiejscowosc";
            this.tbMiejscowosc.Size = new System.Drawing.Size(121, 20);
            this.tbMiejscowosc.TabIndex = 23;
            // 
            // labMiejscowosc
            // 
            this.labMiejscowosc.AutoSize = true;
            this.labMiejscowosc.Location = new System.Drawing.Point(13, 175);
            this.labMiejscowosc.Name = "labMiejscowosc";
            this.labMiejscowosc.Size = new System.Drawing.Size(75, 13);
            this.labMiejscowosc.TabIndex = 22;
            this.labMiejscowosc.Text = "*Miejscowość:";
            // 
            // tbNrLokalu
            // 
            this.tbNrLokalu.Location = new System.Drawing.Point(303, 172);
            this.tbNrLokalu.Name = "tbNrLokalu";
            this.tbNrLokalu.Size = new System.Drawing.Size(52, 20);
            this.tbNrLokalu.TabIndex = 21;
            // 
            // labNrLokalu
            // 
            this.labNrLokalu.AutoSize = true;
            this.labNrLokalu.Location = new System.Drawing.Point(224, 175);
            this.labNrLokalu.Name = "labNrLokalu";
            this.labNrLokalu.Size = new System.Drawing.Size(52, 13);
            this.labNrLokalu.TabIndex = 20;
            this.labNrLokalu.Text = "Nr lokalu:";
            // 
            // tbNrDomu
            // 
            this.tbNrDomu.BackColor = System.Drawing.SystemColors.Info;
            this.tbNrDomu.Enabled = false;
            this.tbNrDomu.Location = new System.Drawing.Point(303, 147);
            this.tbNrDomu.Name = "tbNrDomu";
            this.tbNrDomu.Size = new System.Drawing.Size(52, 20);
            this.tbNrDomu.TabIndex = 19;
            // 
            // labNrDomu
            // 
            this.labNrDomu.AutoSize = true;
            this.labNrDomu.Location = new System.Drawing.Point(224, 150);
            this.labNrDomu.Name = "labNrDomu";
            this.labNrDomu.Size = new System.Drawing.Size(54, 13);
            this.labNrDomu.TabIndex = 18;
            this.labNrDomu.Text = "*Nr domu:";
            // 
            // tbUlica
            // 
            this.tbUlica.BackColor = System.Drawing.SystemColors.Info;
            this.tbUlica.Enabled = false;
            this.tbUlica.Location = new System.Drawing.Point(92, 146);
            this.tbUlica.Name = "tbUlica";
            this.tbUlica.Size = new System.Drawing.Size(121, 20);
            this.tbUlica.TabIndex = 17;
            // 
            // labUlica
            // 
            this.labUlica.AutoSize = true;
            this.labUlica.Location = new System.Drawing.Point(13, 149);
            this.labUlica.Name = "labUlica";
            this.labUlica.Size = new System.Drawing.Size(38, 13);
            this.labUlica.TabIndex = 16;
            this.labUlica.Text = "*Ulica:";
            // 
            // tbGmina
            // 
            this.tbGmina.BackColor = System.Drawing.SystemColors.Info;
            this.tbGmina.Location = new System.Drawing.Point(303, 121);
            this.tbGmina.Name = "tbGmina";
            this.tbGmina.Size = new System.Drawing.Size(121, 20);
            this.tbGmina.TabIndex = 15;
            // 
            // labGmina
            // 
            this.labGmina.AutoSize = true;
            this.labGmina.Location = new System.Drawing.Point(224, 124);
            this.labGmina.Name = "labGmina";
            this.labGmina.Size = new System.Drawing.Size(44, 13);
            this.labGmina.TabIndex = 14;
            this.labGmina.Text = "*Gmina:";
            // 
            // tbPowiat
            // 
            this.tbPowiat.BackColor = System.Drawing.SystemColors.Info;
            this.tbPowiat.Location = new System.Drawing.Point(92, 121);
            this.tbPowiat.Name = "tbPowiat";
            this.tbPowiat.Size = new System.Drawing.Size(121, 20);
            this.tbPowiat.TabIndex = 13;
            // 
            // labPowiat
            // 
            this.labPowiat.AutoSize = true;
            this.labPowiat.Location = new System.Drawing.Point(13, 124);
            this.labPowiat.Name = "labPowiat";
            this.labPowiat.Size = new System.Drawing.Size(46, 13);
            this.labPowiat.TabIndex = 12;
            this.labPowiat.Text = "*Powiat:";
            // 
            // cbWojewodztwo
            // 
            this.cbWojewodztwo.BackColor = System.Drawing.SystemColors.Info;
            this.cbWojewodztwo.Enabled = false;
            this.cbWojewodztwo.FormattingEnabled = true;
            this.cbWojewodztwo.Location = new System.Drawing.Point(303, 94);
            this.cbWojewodztwo.Name = "cbWojewodztwo";
            this.cbWojewodztwo.Size = new System.Drawing.Size(121, 21);
            this.cbWojewodztwo.TabIndex = 11;
            // 
            // labWojewodztwo
            // 
            this.labWojewodztwo.AutoSize = true;
            this.labWojewodztwo.Location = new System.Drawing.Point(224, 97);
            this.labWojewodztwo.Name = "labWojewodztwo";
            this.labWojewodztwo.Size = new System.Drawing.Size(81, 13);
            this.labWojewodztwo.TabIndex = 10;
            this.labWojewodztwo.Text = "*Województwo:";
            // 
            // cbKraj
            // 
            this.cbKraj.BackColor = System.Drawing.SystemColors.Info;
            this.cbKraj.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKraj.Enabled = false;
            this.cbKraj.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbKraj.FormattingEnabled = true;
            this.cbKraj.Location = new System.Drawing.Point(92, 94);
            this.cbKraj.Name = "cbKraj";
            this.cbKraj.Size = new System.Drawing.Size(121, 21);
            this.cbKraj.TabIndex = 9;
            // 
            // labKraj
            // 
            this.labKraj.AutoSize = true;
            this.labKraj.Location = new System.Drawing.Point(13, 97);
            this.labKraj.Name = "labKraj";
            this.labKraj.Size = new System.Drawing.Size(32, 13);
            this.labKraj.TabIndex = 7;
            this.labKraj.Text = "*Kraj:";
            // 
            // tbPelnaNazwa
            // 
            this.tbPelnaNazwa.BackColor = System.Drawing.SystemColors.Info;
            this.tbPelnaNazwa.Location = new System.Drawing.Point(92, 30);
            this.tbPelnaNazwa.Name = "tbPelnaNazwa";
            this.tbPelnaNazwa.ReadOnly = true;
            this.tbPelnaNazwa.Size = new System.Drawing.Size(332, 20);
            this.tbPelnaNazwa.TabIndex = 6;
            // 
            // labPelnaNazwa
            // 
            this.labPelnaNazwa.AutoSize = true;
            this.labPelnaNazwa.Location = new System.Drawing.Point(13, 33);
            this.labPelnaNazwa.Name = "labPelnaNazwa";
            this.labPelnaNazwa.Size = new System.Drawing.Size(77, 13);
            this.labPelnaNazwa.TabIndex = 5;
            this.labPelnaNazwa.Text = "*Pełna nazwa:";
            // 
            // tbRegon
            // 
            this.tbRegon.Location = new System.Drawing.Point(303, 64);
            this.tbRegon.Name = "tbRegon";
            this.tbRegon.Size = new System.Drawing.Size(121, 20);
            this.tbRegon.TabIndex = 4;
            // 
            // labNip
            // 
            this.labNip.AutoSize = true;
            this.labNip.Location = new System.Drawing.Point(13, 67);
            this.labNip.Name = "labNip";
            this.labNip.Size = new System.Drawing.Size(32, 13);
            this.labNip.TabIndex = 1;
            this.labNip.Text = "*NIP:";
            // 
            // labRgon
            // 
            this.labRgon.AutoSize = true;
            this.labRgon.Location = new System.Drawing.Point(227, 67);
            this.labRgon.Name = "labRgon";
            this.labRgon.Size = new System.Drawing.Size(49, 13);
            this.labRgon.TabIndex = 3;
            this.labRgon.Text = "REGON:";
            // 
            // tabFaktury
            // 
            this.tabFaktury.Controls.Add(this.dgFaktury);
            this.tabFaktury.Location = new System.Drawing.Point(4, 22);
            this.tabFaktury.Name = "tabFaktury";
            this.tabFaktury.Size = new System.Drawing.Size(762, 266);
            this.tabFaktury.TabIndex = 1;
            this.tabFaktury.Text = "Faktury";
            this.tabFaktury.UseVisualStyleBackColor = true;
            // 
            // dgFaktury
            // 
            this.dgFaktury.AllowUserToAddRows = false;
            this.dgFaktury.AllowUserToDeleteRows = false;
            this.dgFaktury.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFaktury.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgFaktury.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgFaktury.Location = new System.Drawing.Point(0, 0);
            this.dgFaktury.Name = "dgFaktury";
            this.dgFaktury.Size = new System.Drawing.Size(762, 266);
            this.dgFaktury.TabIndex = 0;
            // 
            // tabPozycjeFaktur
            // 
            this.tabPozycjeFaktur.Controls.Add(this.dgPozycjeFaktur);
            this.tabPozycjeFaktur.Location = new System.Drawing.Point(4, 22);
            this.tabPozycjeFaktur.Name = "tabPozycjeFaktur";
            this.tabPozycjeFaktur.Size = new System.Drawing.Size(762, 266);
            this.tabPozycjeFaktur.TabIndex = 2;
            this.tabPozycjeFaktur.Text = "Pozycje faktur";
            this.tabPozycjeFaktur.UseVisualStyleBackColor = true;
            // 
            // dgPozycjeFaktur
            // 
            this.dgPozycjeFaktur.AllowUserToAddRows = false;
            this.dgPozycjeFaktur.AllowUserToDeleteRows = false;
            this.dgPozycjeFaktur.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPozycjeFaktur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPozycjeFaktur.Location = new System.Drawing.Point(0, 0);
            this.dgPozycjeFaktur.Name = "dgPozycjeFaktur";
            this.dgPozycjeFaktur.ReadOnly = true;
            this.dgPozycjeFaktur.Size = new System.Drawing.Size(762, 266);
            this.dgPozycjeFaktur.TabIndex = 0;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operacjaStatus,
            this.licencjaLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 316);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(770, 22);
            this.statusStrip.TabIndex = 4;
            this.statusStrip.Text = "statusStrip1";
            // 
            // operacjaStatus
            // 
            this.operacjaStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.operacjaStatus.Name = "operacjaStatus";
            this.operacjaStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.operacjaStatus.Size = new System.Drawing.Size(0, 17);
            this.operacjaStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // licencjaLabel
            // 
            this.licencjaLabel.Name = "licencjaLabel";
            this.licencjaLabel.Size = new System.Drawing.Size(755, 17);
            this.licencjaLabel.Spring = true;
            this.licencjaLabel.Text = "WERSJA DEMONSTRACYJNA WAŻNA DO 20.02.2018";
            this.licencjaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(770, 292);
            this.panel1.TabIndex = 6;
            // 
            // openDBF
            // 
            this.openDBF.DefaultExt = "*.dbf";
            this.openDBF.Filter = "Pliki eksportu BM3|*.dbf";
            this.openDBF.Title = "Importuj dane BM3";
            // 
            // openJPK
            // 
            this.openJPK.DefaultExt = "*.xml";
            this.openJPK.Filter = "Pliki JPK|*.xml";
            this.openJPK.Title = "Importuj plik JPK";
            // 
            // saveJPK
            // 
            this.saveJPK.DefaultExt = "*.xml";
            this.saveJPK.Filter = "Pliki JPK|*.xml";
            this.saveJPK.Title = "Zapisz plik JPK";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 338);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(786, 364);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BM3 > JPK_FA";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabJPK.ResumeLayout(false);
            this.gbJPK.ResumeLayout(false);
            this.gbJPK.PerformLayout();
            this.gbDanePodatnika.ResumeLayout(false);
            this.gbDanePodatnika.PerformLayout();
            this.tabFaktury.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgFaktury)).EndInit();
            this.tabPozycjeFaktur.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPozycjeFaktur)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zakończToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabJPK;
        private System.Windows.Forms.TabPage tabFaktury;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripMenuItem wczytajBM3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importujPlikJPKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eksportujPlikJPKToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TabPage tabPozycjeFaktur;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aktywacjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripStatusLabel licencjaLabel;
        private System.Windows.Forms.DataGridView dgFaktury;
        private System.Windows.Forms.DataGridView dgPozycjeFaktur;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem zapiszDaneFirmyToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.GroupBox gbDanePodatnika;
        private System.Windows.Forms.TextBox tbPelnaNazwa;
        private System.Windows.Forms.Label labPelnaNazwa;
        private System.Windows.Forms.TextBox tbRegon;
        private System.Windows.Forms.Label labNip;
        private System.Windows.Forms.Label labRgon;
        private System.Windows.Forms.Label labKraj;
        private System.Windows.Forms.GroupBox gbJPK;
        private System.Windows.Forms.ComboBox cbWaluta;
        private System.Windows.Forms.Label labKodWalutowy;
        private System.Windows.Forms.ComboBox cbUS;
        private System.Windows.Forms.Label labUS;
        private System.Windows.Forms.TextBox tbPoczta;
        private System.Windows.Forms.Label labPoczta;
        private System.Windows.Forms.Label labKodPocztowy;
        private System.Windows.Forms.TextBox tbMiejscowosc;
        private System.Windows.Forms.Label labMiejscowosc;
        private System.Windows.Forms.TextBox tbNrLokalu;
        private System.Windows.Forms.Label labNrLokalu;
        private System.Windows.Forms.TextBox tbNrDomu;
        private System.Windows.Forms.Label labNrDomu;
        private System.Windows.Forms.TextBox tbUlica;
        private System.Windows.Forms.Label labUlica;
        private System.Windows.Forms.TextBox tbGmina;
        private System.Windows.Forms.Label labGmina;
        private System.Windows.Forms.TextBox tbPowiat;
        private System.Windows.Forms.Label labPowiat;
        private System.Windows.Forms.ComboBox cbWojewodztwo;
        private System.Windows.Forms.Label labWojewodztwo;
        private System.Windows.Forms.ComboBox cbKraj;
        private System.Windows.Forms.TextBox tbOkresdo;
        private System.Windows.Forms.Label labokresDo;
        private System.Windows.Forms.TextBox tbOkresOd;
        private System.Windows.Forms.Label labOkres;
        private System.Windows.Forms.TextBox tbWartoscWierszyFakturNetto;
        private System.Windows.Forms.Label labWierszyFakturNetto;
        private System.Windows.Forms.TextBox tbLacznaWartoscFakturBrutto;
        private System.Windows.Forms.Label labLacznaWartoscFakturBrutto;
        private System.Windows.Forms.TextBox tbLiczbaWierszyWFakturach;
        private System.Windows.Forms.Label labLacznaLiczbaWierszyWFakturach;
        private System.Windows.Forms.TextBox tbLiczbaFaktur;
        private System.Windows.Forms.Label labLiczbafaktur;
        private System.Windows.Forms.ToolStripStatusLabel operacjaStatus;
        private System.Windows.Forms.OpenFileDialog openDBF;
        private System.Windows.Forms.OpenFileDialog openJPK;
        private System.Windows.Forms.SaveFileDialog saveJPK;
        private System.Windows.Forms.MaskedTextBox tbNIP;
        private System.Windows.Forms.MaskedTextBox tbKodPocztowy;
        private System.Windows.Forms.ToolStripMenuItem importujKontrahentówToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem walidujPlikJPKToolStripMenuItem;
        private System.Windows.Forms.RadioButton rbBruttoNetto;
        private System.Windows.Forms.RadioButton rbNettoBrutto;
        private System.Windows.Forms.Label labKierunek;
    }
}

