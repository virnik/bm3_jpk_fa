﻿using BM3_JPK_FA.Helper;
using BM3_JPK_FA.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Linq;


namespace BM3_JPK_FA
{
    public partial class MainForm : Form
    {
        private Firma _firma;
        private DataSet _dbfDs;
        private Mapper _mapper;
        private JPK _jpk;
        private List<Firma> _listFirma;
        private readonly DateTime _expireData = new DateTime(2018, 5, 21);
        private List<string> _wojewodztwa = new List<string>() {    "Dolnośląskie",  "Kujawsko-pomorskie",  "Lubelskie",  "Lubuskie",  "Łódzkie",  "Małopolskie",
                                                                    "Mazowieckie",  "Opolskie",  "Podkarpackie",     "Podlaskie",     "Pomorskie",     "Śląskie",
                                                                    "Świętokrzyskie",    "Warmińsko-mazurskie",     "Wielkopolskie",     "Zachodniopomorskie" };

        public MainForm()
        {
            InitializeComponent();
            LoadDataSources();
            _mapper = new Mapper();
            _firma = IsolatedSettingsHelper.PobierzDaneFirmy();
            LoadCompanyData(_firma);
            LoadListFirmData();
            ChangeTabsEnable(false);
            //if (DateTime.Now > _expireData)
            //{
            //    MessageBox.Show("Licencja wygasła! Aplikacja zostanie zamknięta.", "Licencja wygasła", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    Load += (s, e) => Close();
            //    return;
            //}

#if DEBUG
            //var faktury = new List<FakturaTmp>() {
            //            new FakturaTmp(){
            //                AdresKontrahenta ="ul.Kontrahetna 33/3",
            //                Data = DateTime.Now,
            //                NazwaKotrahenta = "Kontrahent S.A",
            //                Nip =  "7619660409",
            //                 NumerDokumentu = 12,
            //                 RodzajDokumentu= "FA",
            //                 PozycjeFaktury= new List<PozycjaFakturyTmp>()
            //                 {
            //                     new PozycjaFakturyTmp()
            //                     {
            //                         CenaPozycjiSprzedazyNetto = 100,
            //                         CenaPozycjiSprzedazyBrutto = 123,
            //                         CenaPozycjiZbytuBrutto = 123,
            //                         CenaPozycjiZbytuNetto = 100,
            //                         JednostkaMiary = "szt",
            //                         LiczbaSztukPozycji=1,
            //                         NazwaArtykulu = "Artykuł pierwszy",
            //                         Vat = 23,
            //                         WartoscSprzedazyNetto = 100,
            //                         WartoscSprzedazyBrutto = 123
            //                     },
            //                     new PozycjaFakturyTmp()
            //                     {
            //                         CenaPozycjiSprzedazyNetto = 50,
            //                         CenaPozycjiSprzedazyBrutto = 61.5m,
            //                         CenaPozycjiZbytuBrutto = 61.5m,
            //                         CenaPozycjiZbytuNetto = 50,
            //                         JednostkaMiary = "szt",
            //                         LiczbaSztukPozycji=10,
            //                         NazwaArtykulu = "Artykuł drugi",
            //                         Vat = 23,
            //                         WartoscSprzedazyNetto = 500,
            //                         WartoscSprzedazyBrutto = 615
            //                     }                     ,
            //                     new PozycjaFakturyTmp()
            //                     {
            //                         CenaPozycjiSprzedazyNetto = 75,
            //                         CenaPozycjiSprzedazyBrutto = 92.5m,
            //                         CenaPozycjiZbytuBrutto = 92.5m,
            //                         CenaPozycjiZbytuNetto = 75,
            //                         JednostkaMiary = "szt",
            //                         LiczbaSztukPozycji=1,
            //                         NazwaArtykulu = "Artykuł trzeci",
            //                         Vat = 23,
            //                         WartoscSprzedazyNetto = 75m,
            //                         WartoscSprzedazyBrutto = 92.25m
            //                     }
            //                 }
            //            },
            //            new FakturaTmp(){
            //                AdresKontrahenta ="ul.Kowalskiego 66/6",
            //                Data = DateTime.Now,
            //                NazwaKotrahenta = "Sklep Kowalski",
            //                Nip =  "1149703123",
            //                 NumerDokumentu = 18,
            //                 RodzajDokumentu= "FA",
            //                 PozycjeFaktury= new List<PozycjaFakturyTmp>()
            //                 {
            //                     new PozycjaFakturyTmp()
            //                     {
            //                         CenaPozycjiSprzedazyNetto = 10,
            //                         CenaPozycjiSprzedazyBrutto = 12.3m,
            //                         CenaPozycjiZbytuBrutto = 12.3m,
            //                         CenaPozycjiZbytuNetto = 10,
            //                         JednostkaMiary = "szt",
            //                         LiczbaSztukPozycji=100,
            //                         NazwaArtykulu = "Produkt 001",
            //                         Vat = 23,
            //                         WartoscSprzedazyNetto = 1000,
            //                         WartoscSprzedazyBrutto = 1230
            //                     },
            //                     new PozycjaFakturyTmp()
            //                     {
            //                         CenaPozycjiSprzedazyNetto = 66,
            //                         CenaPozycjiSprzedazyBrutto = 81.18m,
            //                         CenaPozycjiZbytuBrutto = 81.18m,
            //                         CenaPozycjiZbytuNetto = 66,
            //                         JednostkaMiary = "szt",
            //                         LiczbaSztukPozycji=12,
            //                         NazwaArtykulu = "Produkt 002",
            //                         Vat = 23,
            //                         WartoscSprzedazyNetto = 792,
            //                         WartoscSprzedazyBrutto = 974.16m
            //                     }
            //                 }
            //            }
            //            };
            //_jpk = _mapper.MappJPK(faktury, _firma);

            //RefreshGuiJpkData();
            //ChangeTabsEnable(true);
            //operacjaStatus.Text = "Wczytano i przetworzono dane z BM3...";
#endif

            licencjaLabel.Text = "PEŁNA WERSJA";
            this.tbNIP.TextChanged += new System.EventHandler(this.tbNIP_TextChanged);
        }

        private void CloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void LoadDataSources()
        {
            cbWaluta.DataSource = Enum.GetValues(typeof(currCode_Type));
            cbKraj.DataSource = Enum.GetValues(typeof(TKodKraju));
            cbWojewodztwo.DataSource = _wojewodztwa;
            cbUS.DataSource = KodUsTmpDataSource.KodyUS;
        }

        private void ClearFirmBindings()
        {
            tbGmina.DataBindings.Clear();
            tbKodPocztowy.DataBindings.Clear();
            tbMiejscowosc.DataBindings.Clear();
            tbNIP.DataBindings.Clear();
            tbNrDomu.DataBindings.Clear();
            tbNrLokalu.DataBindings.Clear();
            tbPelnaNazwa.DataBindings.Clear();
            tbPoczta.DataBindings.Clear();
            tbPowiat.DataBindings.Clear();
            tbRegon.DataBindings.Clear();
            tbUlica.DataBindings.Clear();
            cbKraj.DataBindings.Clear();
            cbUS.DataBindings.Clear();
            cbWaluta.DataBindings.Clear();
            cbWojewodztwo.DataBindings.Clear();
        }

        private bool IsLicense(Firma firma)
        {
            if (!_listFirma.Any(f => f.NIP == (firma?.NIP ?? string.Empty)))
            {
                MessageBox.Show("Brak licencji dla podanego numeru NIP!", "Brak licencji!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
                return true;
        }

        private void LoadCompanyData(Firma firma)
        {
            try
            {
                tbGmina.DataBindings.Add("Text", firma, "Gmina", false, DataSourceUpdateMode.OnPropertyChanged);
                tbKodPocztowy.DataBindings.Add("Text", firma, "KodPocztowy", false, DataSourceUpdateMode.OnPropertyChanged);
                tbMiejscowosc.DataBindings.Add("Text", firma, "Miejscowosc", false, DataSourceUpdateMode.OnPropertyChanged);
                tbNIP.DataBindings.Add("Text", firma, "NIP", false, DataSourceUpdateMode.OnPropertyChanged);
                tbNrDomu.DataBindings.Add("Text", firma, "NrDomu", false, DataSourceUpdateMode.OnPropertyChanged);
                tbNrLokalu.DataBindings.Add("Text", firma, "NrLokalu", false, DataSourceUpdateMode.OnPropertyChanged);
                tbPelnaNazwa.DataBindings.Add("Text", firma, "PelnaNazwa", false, DataSourceUpdateMode.OnPropertyChanged);
                tbPoczta.DataBindings.Add("Text", firma, "Poczta", false, DataSourceUpdateMode.OnPropertyChanged);
                tbPowiat.DataBindings.Add("Text", firma, "Powiat", false, DataSourceUpdateMode.OnPropertyChanged);
                tbRegon.DataBindings.Add("Text", firma, "REGON", false, DataSourceUpdateMode.OnPropertyChanged);
                tbUlica.DataBindings.Add("Text", firma, "Ulica", false, DataSourceUpdateMode.OnPropertyChanged);
                cbKraj.DataBindings.Add("Text", firma, "Kraj", false, DataSourceUpdateMode.OnPropertyChanged);
                cbUS.DataBindings.Add("SelectedValue", firma, "UrzadSkarbowy", false, DataSourceUpdateMode.OnPropertyChanged);
                cbWaluta.DataBindings.Add("Text", firma, "KodWalutowy", false, DataSourceUpdateMode.OnPropertyChanged);
                cbWojewodztwo.DataBindings.Add("Text", firma, "Wojewodztwo", false, DataSourceUpdateMode.OnPropertyChanged);
                rbNettoBrutto.Checked = firma.KierunekPrzeliczania == KierunekPrzeliczania.NettoBrutto;
                rbBruttoNetto.Checked = firma.KierunekPrzeliczania == KierunekPrzeliczania.BruttoNetto;
                //firma.UrzadSkarbowy = "2405";
            }
            catch (Exception e)
            {
                ShowErrorMessage(e.ToString());
                throw e;
            }
        }

        private void ShowErrorMessage(string ex)
        {
            MessageBox.Show("Wystąpił niespodziewany błąd krytyczny. \r\nZgłoś go wraz z plikiem logs.log dostawcy aplikacji.", "Błąd krytyczny!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ZapiszDaneFirmyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsValidateCompanyData())
            {
                if (IsLicense(_firma))
                {
                    _firma.KierunekPrzeliczania = rbNettoBrutto.Checked ? KierunekPrzeliczania.NettoBrutto : KierunekPrzeliczania.BruttoNetto;
                    IsolatedSettingsHelper.ZapiszDaneFirmy(_firma);
                    ActualizeAddressInJPK(_firma);
                    operacjaStatus.Text = "Zapisano dane firmy...";
                }
            }
            else
                MessageBox.Show("Uzupełnij wymagane pola!", "Brak wymaganych pól", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void ActualizeAddressInJPK(Firma firma)
        {
            if (_jpk != null)
            {
                //_jpk.Podmiot1.AdresPodmiotu.Gmina = firma.Gmina;
                //_jpk.Podmiot1.AdresPodmiotu.KodPocztowy = firma.KodPocztowy;
                //_jpk.Podmiot1.AdresPodmiotu.Miejscowosc = firma.Miejscowosc;
                //_jpk.Podmiot1.AdresPodmiotu.NrDomu = firma.NrDomu;
                //_jpk.Podmiot1.AdresPodmiotu.NrLokalu = firma.NrLokalu;
                //_jpk.Podmiot1.AdresPodmiotu.Poczta = firma.Poczta;
                //_jpk.Podmiot1.AdresPodmiotu.Powiat = firma.Powiat;
                //_jpk.Podmiot1.AdresPodmiotu.Ulica = firma.Ulica;
                //_jpk.Podmiot1.AdresPodmiotu.Wojewodztwo = firma.Wojewodztwo;
            }
        }


        private void LoadAndProcessDBF(string directory, string file)
        {
            try
            {
                string constr = $@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={directory};Extended Properties=dBASE IV;User ID=Admin;Password=;";
                using (OleDbConnection con = new OleDbConnection(constr))
                {
                    var sql = $@"select * from {file} where (DOKUMENT = 'Fa' OR DOKUMENT = 'ZF')";// AND NR_DOKUM = 6";
                    OleDbCommand cmd = new OleDbCommand(sql, con);
                    con.Open();
                    _dbfDs = new DataSet(); ;
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(_dbfDs);

                    var faktury = _mapper.DataSetToListFaktur(_dbfDs);
                    _jpk = _mapper.MappJPK(faktury, _firma);

                    RefreshGuiJpkData();
                    ChangeTabsEnable(true);
                    operacjaStatus.Text = "Wczytano i przetworzono dane z BM3...";
                }
            }
            catch (Exception e)
            {
                ShowErrorMessage(e.ToString());
                throw e;
            }
        }

        private void LoadAndProcessDBFKontrahentow(string directory, string file)
        {
            try
            {
                string constr = $@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={directory};Extended Properties=dBASE IV;User ID=Admin;Password=;";
                using (OleDbConnection con = new OleDbConnection(constr))
                {
                    var sql = $@"select distinct NAZWA1, NAZWA2, NIP, SYMBOL from {file} ";
                    OleDbCommand cmd = new OleDbCommand(sql, con);
                    con.Open();
                    _dbfDs = new DataSet(); 
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(_dbfDs);

                    foreach (DataRow dataRow in _dbfDs.Tables[0].Rows)
                    {
                        var faktury = _jpk.Faktura.Where(f => !string.IsNullOrEmpty(f.P_3A) && f.P_3A == dataRow.Field<string>("SYMBOL")?.Replace("-", string.Empty)).ToList();  
                        if (faktury?.Any() == true)
                        {
                            foreach (var faktura in faktury)
                            {
                                faktura.P_3A = dataRow.Field<string>("NAZWA1")?.Replace("*", string.Empty) + dataRow.Field<string>("NAZWA2")?.Replace("*", string.Empty);
                            }
                        }
                    }
                    RefreshGuiJpkData();
                    ChangeTabsEnable(true);
                    operacjaStatus.Text = "Wczytano i przetworzono kontrahentów...";
                }
            }
            catch (Exception e)
            {
                ShowErrorMessage(e.ToString());
                throw e;
            }
        }

        private void wczytajBM3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsValidateCompanyData())
            {
                if (IsLicense(_firma))
                    if (openDBF.ShowDialog() == DialogResult.OK)
                    {
                        Cursor cursor = Cursor.Current;
                        Cursor.Current = Cursors.WaitCursor;
                        LoadAndProcessDBF(Path.GetDirectoryName(openDBF.FileName), openDBF.SafeFileName);
                        Cursor.Current = Cursors.Default;
                    }
            }
            else
                MessageBox.Show("Przed wczytaniem pliku uzupełnij i zapisz dane firmy!", "Brak wymaganych pól", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void eksportujPlikJPKToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                saveJPK.FileName = "JPK_" + DateTime.Now.AddMonths(-1).ToString("MMyyy");
                if (IsLicense(_firma))
                    if (saveJPK.ShowDialog() == DialogResult.OK)
                    {
                        var serializer = new XmlSerializer(typeof(JPK));
                        using (var stream = new StreamWriter(saveJPK.FileName))
                            serializer.Serialize(stream, _jpk);
                        operacjaStatus.Text = "Zapisano plik JPK...";
                    }
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.ToString());
                throw ex;
            }

        }

        private void tabControler_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (!e.TabPage.Enabled)
                MessageBox.Show("Wczytaj plik BM3", "Wczytaj plik", MessageBoxButtons.OK, MessageBoxIcon.Information);

            e.Cancel = !e.TabPage.Enabled;
        }

        private void importujPlikJPKToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsLicense(_firma) && openJPK.ShowDialog() == DialogResult.OK)
                {
                    var serializer = new XmlSerializer(typeof(JPK));
                    using (StreamReader reader = new StreamReader(openJPK.FileName))
                    {
                        _jpk = (JPK)serializer.Deserialize(reader);
                        RefreshGuiJpkData();
                        ChangeTabsEnable(true);
                        operacjaStatus.Text = "Zaimportowano plik JPK...";

                    }
                }
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.ToString());
                throw ex;
            }
        }

        private void ChangeTabsEnable(bool en)
        {
            this.tabFaktury.Enabled = this.tabPozycjeFaktur.Enabled = eksportujPlikJPKToolStripMenuItem.Enabled = en;
            if (!en) tabControl.SelectedIndex = 0;
        }

        private void RefreshGuiJpkData()
        {
            dgFaktury.DataSource = _jpk.Faktura;
            dgPozycjeFaktur.DataSource = _jpk.FakturaWiersz;
            tbOkresOd.Text = _jpk.Naglowek.DataOd.ToShortDateString();
            tbOkresdo.Text = _jpk.Naglowek.DataDo.ToShortDateString();
            tbLiczbaFaktur.Text = _jpk.FakturaCtrl.LiczbaFaktur;
            tbLacznaWartoscFakturBrutto.Text = _jpk.FakturaCtrl.WartoscFaktur.ToString();
            tbLiczbaWierszyWFakturach.Text = _jpk.FakturaWierszCtrl.LiczbaWierszyFaktur;
            tbWartoscWierszyFakturNetto.Text = _jpk.FakturaWierszCtrl.WartoscWierszyFaktur.ToString();

            foreach (DataGridViewColumn column in dgFaktury.Columns)
            {
                if (column.Name != "PrzyczynaKorekty")
                    column.ReadOnly = true;
            }
        }

        private bool IsValidateCompanyData()
        {
            List<Control> c = gbDanePodatnika.Controls.OfType<TextBox>().Cast<Control>().Where(f => f.BackColor == SystemColors.Info).ToList();
            c.AddRange(gbDanePodatnika.Controls.OfType<ComboBox>().Cast<Control>().Where(f => f.BackColor == SystemColors.Info).ToList());
            c.AddRange(gbDanePodatnika.Controls.OfType<MaskedTextBox>().Cast<Control>().Where(f => f.BackColor == SystemColors.Info).ToList());
            c.AddRange(gbJPK.Controls.OfType<ComboBox>().Cast<Control>().Where(f => f.BackColor == SystemColors.Info).ToList());
            foreach (var control in c)
            {
                if (string.IsNullOrEmpty(control.Text))
                    return false;
            }
            return true;
        }

        private void LoadListFirmData()
        {
            _listFirma = new List<Firma>() {
                new Firma(){
                      PelnaNazwa = "FAUNA I FLORA Firma Ogrodniczo Zoologiczna Banasiak Mariusz",
                      KodPocztowy =  "41-700",
                    Miejscowosc ="Ruda Śląska",
                      Ulica = "Matejki",
                    NrDomu ="16",
                    NIP ="6261004106",
                    KodWalutowy ="PLN",
                    Kraj ="PL",
                      UrzadSkarbowy="2405",
                      Wojewodztwo="Śląskie"
                },
                new Firma(){
                      PelnaNazwa = "Inter-Complex  Mirosława Krzeszowiec",
                      KodPocztowy =  "44-100",
                    Miejscowosc ="Gliwice",
                      Ulica = "Piastów",
                    NrDomu ="6a",
                    NIP ="6311710592",
                    KodWalutowy ="PLN",
                    Kraj ="PL",
                      UrzadSkarbowy="",
                      Wojewodztwo="Śląskie"
                },
                new Firma(){
                      PelnaNazwa = "Hurtownia Nabiałowo-Spożywcza Beata Szpak",
                      KodPocztowy =  "41-902",
                    Miejscowosc ="Bytom",
                      Ulica = "Chrzanowskiego",
                    NrDomu ="23a",
                    NIP ="6260011877",
                    KodWalutowy ="PLN",
                    Kraj ="PL",
                      UrzadSkarbowy="2405",
                      Wojewodztwo="Śląskie"
                },
                new Firma(){
                      PelnaNazwa = "\"TTNet\" spółka cywilna Tomasz Wiszyński Tomasz Jurkowski",
                      KodPocztowy =  "41-902",
                    Miejscowosc ="Bytom",
                      Ulica = "Strzelców Bytomskich",
                    NrDomu ="27a",
                    NIP ="6262963470",
                    KodWalutowy ="PLN",
                    Kraj ="PL",
                      UrzadSkarbowy="2405",
                      Wojewodztwo="Śląskie"
                },
                new Firma(){
                      PelnaNazwa = "\"Ondraszek\" Andrzej Widera",
                      KodPocztowy =  "41-933",
                    Miejscowosc ="Bytom",
                      Ulica = "Legnicka",
                    NrDomu ="3",
                    NIP ="6261065088",
                    KodWalutowy ="PLN",
                    Kraj ="PL",
                      UrzadSkarbowy="2405",
                      Wojewodztwo="Śląskie"
                },
                new Firma(){
                      PelnaNazwa = "\"PET-FLORA\" Joanna Banasiak",
                      KodPocztowy =  "41-907",
                    Miejscowosc ="Bytom",
                      Ulica = "Wyzwolenia",
                    NrDomu ="127b",
                    NIP ="6261006074",
                    KodWalutowy ="PLN",
                    Kraj ="PL",
                      UrzadSkarbowy="2405",
                      Wojewodztwo="Śląskie"
                },
                new Firma(){
                      PelnaNazwa = "Spółka Cywilna KUPIEC Mariusz Banasiak i Wojciech Plewaniuk",
                      KodPocztowy =  "41-902",
                    Miejscowosc ="Bytom",
                      Ulica = "Korfantego",
                    NrDomu ="28",
                    NIP ="6261006140",
                    KodWalutowy ="PLN",
                    Kraj ="PL",
                      UrzadSkarbowy="2405",
                      Wojewodztwo="Śląskie"
                }
            };
        }

        private void tbNIP_TextChanged(object sender, EventArgs e)
        {
            if (sender is MaskedTextBox tb)
            {
                if (tb.Text == _firma.NIP) return;
                if (tb.Text.Length == 10)
                {
                    var firma = _listFirma.FirstOrDefault(f => f.NIP == tb.Text);
                    if (firma != null)
                    {
                        ClearFirmBindings();
                        _firma = new Firma(firma);
                        LoadCompanyData(_firma);
                    }
                    else
                        MessageBox.Show("Brak licencji dla podanego numeru NIP!", "Brak licencji!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"BM3>JPK_FA v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + @" 

Aplikacja generująca pliki JPK_FA v3 na podstawie danych z pliku eksportu programu księgowego BM3.", "O aplikacji", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void aktywacjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Jak wygenerować plik JPK_FA?

1.Uzupełnij NIP firmy dla której wykupiono licencję aplikacji 
2.Wybierz urząd skarbowy i walute,
2.Zapisz dane firmy (Plik>Zapisz | F6),
3.Importuj dane z pliku progamu BM3 (Plik>Importuj dane BM3 | F2)
4.W zakładce Faktury uzupełnij powody korekt (domyślnie zwrot)
5.Zapisz plik JPK_FA (Plik>Eksportuj plik JPK | F4)", "Jak generować JPK_FA?", MessageBoxButtons.OK, MessageBoxIcon.None);

        }

        private void importujKontrahentowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsValidateCompanyData())
            {
                if (IsLicense(_firma) && _jpk?.Faktura != null)
                {
                    if (openDBF.ShowDialog() == DialogResult.OK)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        LoadAndProcessDBFKontrahentow(Path.GetDirectoryName(openDBF.FileName), openDBF.SafeFileName);
                        Cursor.Current = Cursors.Default;
                    }
                }
                else
                {
                    MessageBox.Show("Przed wczytaniem pliku kontrahentów wczytaj plik DBF z fakturami!", "Przed wczytaniem pliku kontrahentów wczytaj plik DBF z fakturami!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
                MessageBox.Show("Przed wczytaniem pliku kontrahentów wczytaj plik DBF z fakturami!", "Brak wymaganych pól", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void walidujPlikJPKToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openJPK.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                WalidatorJPK.Validate(openJPK.FileName);
             //   LoadAndProcessDBFKontrahentow(Path.GetDirectoryName(openDBF.FileName), openDBF.SafeFileName);
                Cursor.Current = Cursors.Default;
            }
        }

        private void rbBruttoNetto_CheckedChanged(object sender, EventArgs e)
        {
            if(_firma != null)
            {
                _firma.KierunekPrzeliczania = rbBruttoNetto.Checked ? KierunekPrzeliczania.BruttoNetto : KierunekPrzeliczania.NettoBrutto;
            }
        }
    }
}
