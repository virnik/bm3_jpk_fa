﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM3_JPK_FA.Model
{
    public class PozycjaFakturyTmp
    {
        public string NazwaArtykulu { get; set; }
        public decimal CenaPozycjiZbytuNetto { get; set; }
        public decimal CenaPozycjiZbytuBrutto { get; set; }
        public decimal CenaPozycjiSprzedazyNetto { get; set; }
        public decimal CenaPozycjiSprzedazyBrutto { get; set; }
        public decimal LiczbaSztukPozycji { get; set; }
        public string JednostkaMiary { get; set; }
        public int Vat { get; set; }
        public bool CzyUpust
        {
            get
            {
                return CenaPozycjiZbytuNetto != CenaPozycjiSprzedazyNetto;
            }
        }
        public decimal KwotaUlgiNetto
        {
            get
            {
                return CenaPozycjiZbytuNetto - CenaPozycjiSprzedazyNetto;
            }
        }

        public decimal ProcentUpustu
        {
            get
            {
                if (CzyUpust)
                {
                    return 100 - Math.Round(CenaPozycjiSprzedazyNetto * 100 / CenaPozycjiZbytuNetto);
                }
                return 0m;
            }
        }

        public decimal WartoscSprzedazyBrutto
        {
            get; set;
        }

        public decimal WartoscSprzedazyBruttoPrzelicz_NB
        {
            get
            {
                var przelicz = WartoscSprzedazyNetto + (WartoscSprzedazyNetto * (Vat / 100.00m));
                return decimal.Parse(string.Format("{0:000.00}", Math.Truncate((przelicz * 100) / 100)));
            }
        }

        public decimal WartoscSprzedazyNetto
        {
            get; set;
        }
        public decimal KwotaVAT
        {
            get
            {
                return WartoscSprzedazyBrutto - WartoscSprzedazyNetto;
            }
        }
        public JPKFakturaWierszP_12 VatEnum
        {
            get
            {
                switch (Vat)
                {
                    case 0: return JPKFakturaWierszP_12.Item0;
                    case 3: return JPKFakturaWierszP_12.Item3;
                    case 4: return JPKFakturaWierszP_12.Item4;
                    case 5: return JPKFakturaWierszP_12.Item5;
                    case 7: return JPKFakturaWierszP_12.Item7;
                    case 8: return JPKFakturaWierszP_12.Item8;
                    case 22: return JPKFakturaWierszP_12.Item22;
                    case 23: return JPKFakturaWierszP_12.Item23;
                    default:
                        return JPKFakturaWierszP_12.Item0;
                }
            }
        }
    }
}
