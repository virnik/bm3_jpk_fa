﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM3_JPK_FA.Model
{
    public class FakturaTmp
    {
        public DateTime Data { get; set; }
        public string RodzajDokumentu { get; set; }
        public double NumerDokumentu { get; set; }
        public string NazwaKotrahenta { get; set; }
        public string NrFakturyKorygowanej { get; set; }

        #region brutto -> netto
        public decimal KwotaFakturyNetto23_BN
        {
            get
            {
                return Math.Round(PozycjeFaktury.Where(w => w.Vat == 23).Sum(p => p.WartoscSprzedazyBrutto) / (123m / 100m), 2);
            }
        }
        public decimal KwotaFakturyNetto8_BN
        {
            get
            {
                return Math.Round(PozycjeFaktury.Where(w => w.Vat == 8).Sum(p => p.WartoscSprzedazyBrutto) / (108m / 100m), 2);
            }
        }
        public decimal KwotaFakturyNetto5_BN
        {
            get
            {
                return Math.Round(PozycjeFaktury.Where(w => w.Vat == 5).Sum(p => p.WartoscSprzedazyBrutto) / (105m / 100m), 2);
            }
        }
        public decimal KwotaFakturyBrutto23_BN
        {
            get => PozycjeFaktury.Where(w => w.Vat == 23).Sum(p => p.WartoscSprzedazyBrutto);

        }
        public decimal KwotaFakturyBrutto8_BN
        {
            get => PozycjeFaktury.Where(w => w.Vat == 8).Sum(p => p.WartoscSprzedazyBrutto);

        }
        public decimal KwotaFakturyBrutto5_BN
        {
            get => PozycjeFaktury.Where(w => w.Vat == 5).Sum(p => p.WartoscSprzedazyBrutto);

        }

        public decimal KwotaVat23_BN { get { return KwotaFakturyBrutto23_BN - KwotaFakturyNetto23_BN; /*PozycjeFaktury.Where(w => w.Vat == 23).Sum(p => p.KwotaVAT);*/ } }
        public decimal KwotaVat8_BN { get { return KwotaFakturyBrutto8_BN - KwotaFakturyNetto8_BN; /*PozycjeFaktury.Where(w => w.Vat == 8).Sum(p => p.KwotaVAT);*/ } }
        public decimal KwotaVat5_BN { get { return KwotaFakturyBrutto5_BN - KwotaFakturyNetto5_BN;/* PozycjeFaktury.Where(w => w.Vat == 5).Sum(p => p.KwotaVAT);*/ } }
        #endregion

        #region netto -> brutto
        public decimal KwotaFakturyNetto23_NB
        {
            get
            {
                return PozycjeFaktury.Where(w => w.Vat == 23).Sum(p => p.WartoscSprzedazyNetto);
            }
        }
        public decimal KwotaFakturyNetto8_NB
        {
            get
            {
                return PozycjeFaktury.Where(w => w.Vat == 8).Sum(p => p.WartoscSprzedazyNetto);
            }
        }
        public decimal KwotaFakturyNetto5_NB
        {
            get
            {
                return PozycjeFaktury.Where(w => w.Vat == 5).Sum(p => p.WartoscSprzedazyNetto);
            }
        }
        public decimal KwotaFakturyBrutto23_NB
        {
            get => KwotaFakturyNetto23_NB + KwotaVat23_NB;

        }
        public decimal KwotaFakturyBrutto8_NB
        {
            get => KwotaFakturyNetto8_NB + KwotaVat8_NB;

        }
        public decimal KwotaFakturyBrutto5_NB
        {
            get => KwotaFakturyNetto5_NB + KwotaVat5_NB;

        }

        public decimal KwotaVat23_NB { get { return decimal.Parse(string.Format("{0:000.00}", Math.Round((KwotaFakturyNetto23_NB * 23 / 100.00m), 2, MidpointRounding.AwayFromZero))); } }
        public decimal KwotaVat8_NB { get { return decimal.Parse(string.Format("{0:000.00}", Math.Round((KwotaFakturyNetto8_NB * 8 / 100.00m), 2, MidpointRounding.AwayFromZero))); } }
        public decimal KwotaVat5_NB { get { return decimal.Parse(string.Format("{0:000.00}", Math.Round((KwotaFakturyNetto5_NB * 5 / 100.00m), 2, MidpointRounding.AwayFromZero))); } }
        #endregion

        public decimal SumaLiczby
        {
            get
            {
                return PozycjeFaktury.Sum(p => p.LiczbaSztukPozycji);
            }
        }
        public string AdresKontrahenta { get; set; }
        public string Nip { get; set; }
        public string NumerFakturyCaly { get { return RodzajDokumentu + NumerDokumentu + "/" + Data.ToString("%M") + "/" + Data.ToString("yy"); } }
        public List<PozycjaFakturyTmp> PozycjeFaktury { get; set; } = new List<PozycjaFakturyTmp>();

    }
}
