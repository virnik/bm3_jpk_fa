﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM3_JPK_FA.Model
{
    public class KodUSTmp
    {
        public string NazwaUS { get; set; }
        public string Kod { get; set; }
    }
}
