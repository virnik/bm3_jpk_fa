﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM3_JPK_FA.Model
{
    public class Firma
    {
        public string NIP { get; set; }
        public string REGON { get; set; }
        public string PelnaNazwa { get; set; }
        public string Kraj { get; set; }
        public string Wojewodztwo { get; set; }
        public string Powiat { get; set; }
        public string Gmina { get; set; }
        public string Ulica { get; set; }
        public string NrDomu { get; set; }
        public string NrLokalu { get; set; }
        public string Miejscowosc { get; set; }
        public string Poczta { get; set; }
        public string KodPocztowy { get; set; }
        public string UrzadSkarbowy { get; set; }
        public string KodWalutowy { get; set; }

        public KierunekPrzeliczania KierunekPrzeliczania { get; set; }

        public string PelnyAdres
        {
            get
            {
                return Ulica + " " + NrDomu + " " + NrLokalu + " " + KodPocztowy + " " + Miejscowosc;
            }
        }

        public Firma(Firma firma)
        {
            this.Gmina = firma.Gmina;
            this.KodPocztowy = firma.KodPocztowy;
           // this.KodWalutowy = firma.KodWalutowy;
            this.Kraj = firma.Kraj;
            this.Miejscowosc = firma.Miejscowosc;
            this.NIP = firma.NIP;
            this.NrDomu = firma.NrDomu;
            this.NrLokalu = firma.NrLokalu;
            this.PelnaNazwa = firma.PelnaNazwa;
            this.Powiat = firma.Powiat;
            this.REGON = firma.REGON;
            this.Ulica = firma.Ulica;
           // this.UrzadSkarbowy = firma.UrzadSkarbowy;
            this.Wojewodztwo = firma.Wojewodztwo;
            this.KierunekPrzeliczania = firma.KierunekPrzeliczania;
        }

        public Firma()
        {

        }
    }
}
