﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace BM3_JPK_FA.Helper
{
    public static class WalidatorJPK
    {
        private static bool isValidated;
        public static void Validate(string jpkPath)
        {
            var path = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)).LocalPath;
            XmlSchemaSet schema = new XmlSchemaSet();
            schema.Add("http://jpk.mf.gov.pl/wzor/2019/09/27/09271/", $"{path}\\schematy\\Faktury_VAT_-JPK_FA(3).xsd");
            using (XmlReader rd = XmlReader.Create(jpkPath))
            {
                isValidated = true;
                XDocument doc = XDocument.Load(rd);
                doc.Validate(schema, ValidationEventHandler, true);
                if (isValidated)
                    MessageBox.Show("Plik zgodny ze schematem XSD.", "Walidacja zakończona", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (Enum.TryParse("Error", out XmlSeverityType type))
            {
                if (type == XmlSeverityType.Error)
                {
                    isValidated = false;
                    MessageBox.Show("Błąd walidacji pliku jpk:\r\n" + e.Message, "Błąd walidacji", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}