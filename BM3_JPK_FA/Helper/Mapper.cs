﻿using BM3_JPK_FA.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM3_JPK_FA.Helper
{
    public class Mapper
    {

        public List<FakturaTmp> DataSetToListFaktur(DataSet ds)
        {
            var listaFaktur = new List<FakturaTmp>();

            foreach (DataRow dataRow in ds.Tables[0].Rows)
            {
                FakturaTmp faktura = listaFaktur.SingleOrDefault(f => f.NumerDokumentu == dataRow.Field<double>("NR_DOKUM") && f.RodzajDokumentu == dataRow.Field<string>("DOKUMENT"));
                if (faktura == null)
                {
                    faktura = MappFaktura(dataRow);
                    listaFaktur.Add(faktura);
                }
                faktura.PozycjeFaktury.Add(MappPozycjaFaktury(dataRow));
            }

            return listaFaktur;
        }

        private PozycjaFakturyTmp MappPozycjaFaktury(DataRow dataRow)
        {
            return new PozycjaFakturyTmp()
            {
                NazwaArtykulu = dataRow.Field<string>("NAZWA_ART"),
                CenaPozycjiSprzedazyNetto = Convert.ToDecimal(dataRow.Field<double>("CENA_SPR_N")),
                CenaPozycjiSprzedazyBrutto = Convert.ToDecimal(dataRow.Field<double>("CENA_SPR_B")),
                CenaPozycjiZbytuNetto = Convert.ToDecimal(dataRow.Field<double>("CENA_ZBT_N")),
                CenaPozycjiZbytuBrutto = Convert.ToDecimal(dataRow.Field<double>("CENA_ZBT_B")),
                //WRT_SPR_BR
                WartoscSprzedazyBrutto = Convert.ToDecimal(dataRow.Field<double>("WRT_SPR_BR")),
                WartoscSprzedazyNetto = Convert.ToDecimal(dataRow.Field<double>("WRT_SPR_NE")),
                LiczbaSztukPozycji = Convert.ToDecimal(dataRow.Field<double>("ILOSC")), // double !!!
                JednostkaMiary = dataRow.Field<string>("JM"),
                Vat = Convert.ToInt32(dataRow.Field<string>("VAT").Replace("%", string.Empty))
            };
        }

        private FakturaTmp MappFaktura(DataRow dataRow)
        {
            return new FakturaTmp()
            {
                Data = DateTime.ParseExact(dataRow.Field<DateTime>("DATA").ToString("dd.MM.yyyy"), "dd.MM.yyyy", CultureInfo.InvariantCulture),
                NazwaKotrahenta = dataRow.Field<string>("SYMB_KONTR"),
                RodzajDokumentu = dataRow.Field<string>("DOKUMENT"),
                NumerDokumentu = dataRow.Field<double>("NR_DOKUM"),
                AdresKontrahenta = dataRow.Field<string>("ADRES_KNTR"),
                Nip = dataRow.Field<string>("NIP"),
                NrFakturyKorygowanej = dataRow.Field<string>("SYMBOL_DOK")
            };
        }

        public JPK MappJPK(List<FakturaTmp> faktury, Firma firma)
        {
            var jpk = new JPK();
            var listaFaktur = new List<JPKFaktura>();
            var pozycjeFaktur = new List<JPKFakturaWiersz>();
            DateTime date = DateTime.Now;
            foreach (FakturaTmp faktura in faktury)
            {
                //wersja testowa, maks 5 katur
                //   if (i == 10) break;
                // i++;
                JPKFakturaRodzajFaktury rodzaj;
                switch (faktura.RodzajDokumentu)
                {
                    case "Fa":
                        rodzaj = JPKFakturaRodzajFaktury.VAT;
                        break;
                    case "ZF":
                        rodzaj = JPKFakturaRodzajFaktury.KOREKTA;
                        break;
                    case "Za":
                        rodzaj = JPKFakturaRodzajFaktury.ZAL;
                        break;
                    default:
                        rodzaj = JPKFakturaRodzajFaktury.VAT;
                        break;
                }
                var nowaFaktura = new JPKFaktura()
                {
                    RodzajFaktury = rodzaj,
                    P_1 = faktura.Data,
                    P_2A = faktura.NumerFakturyCaly,
                    P_3A = faktura.NazwaKotrahenta.Replace("*", string.Empty),
                    P_3B = faktura.AdresKontrahenta,
                    P_3C = firma.PelnaNazwa,
                    P_3D = firma.PelnyAdres,
                    P_5A = MSCountryCode_Type.PL,
                    P_5ASpecified = true,
                    P_5B = faktura.Nip?.Replace("-", string.Empty),
                    P_4B = firma.NIP.Replace("-", string.Empty),
                    KodWaluty = currCode_Type.PLN, //firma.KodWalutowy,
                    P_13_1 = firma.KierunekPrzeliczania == KierunekPrzeliczania.NettoBrutto ? faktura.KwotaFakturyNetto23_NB : faktura.KwotaFakturyNetto23_BN,
                    P_13_2 = firma.KierunekPrzeliczania == KierunekPrzeliczania.NettoBrutto ? faktura.KwotaFakturyNetto8_NB : faktura.KwotaFakturyNetto8_BN,
                    P_13_3 = firma.KierunekPrzeliczania == KierunekPrzeliczania.NettoBrutto ? faktura.KwotaFakturyNetto5_NB : faktura.KwotaFakturyNetto5_BN,
                    P_14_1 = firma.KierunekPrzeliczania == KierunekPrzeliczania.NettoBrutto ? faktura.KwotaVat23_NB : faktura.KwotaVat23_BN,
                    P_14_2 = firma.KierunekPrzeliczania == KierunekPrzeliczania.NettoBrutto ? faktura.KwotaVat8_NB : faktura.KwotaVat8_BN,
                    P_14_3 = firma.KierunekPrzeliczania == KierunekPrzeliczania.NettoBrutto ? faktura.KwotaVat5_NB : faktura.KwotaVat5_BN,
                    P_15 = (firma.KierunekPrzeliczania == KierunekPrzeliczania.NettoBrutto ? faktura.KwotaFakturyBrutto23_NB : faktura.KwotaFakturyBrutto23_BN)
                    + (firma.KierunekPrzeliczania == KierunekPrzeliczania.NettoBrutto ? faktura.KwotaFakturyBrutto5_NB : faktura.KwotaFakturyBrutto5_BN)
                    + (firma.KierunekPrzeliczania == KierunekPrzeliczania.NettoBrutto ? faktura.KwotaFakturyBrutto8_NB : faktura.KwotaFakturyBrutto8_BN),
                    P_22A = new DateTime(date.Year, date.Month, 1).AddMonths(-1),
                    P_16 = false,
                    P_17 = false,
                    P_18 = false //???
                };
                if (rodzaj == JPKFakturaRodzajFaktury.KOREKTA)
                    nowaFaktura.OkresFaKorygowanej = faktura.Data.Year.ToString();
                if (rodzaj == JPKFakturaRodzajFaktury.KOREKTA)
                {
                    nowaFaktura.PrzyczynaKorekty = "zwrot";
                    nowaFaktura.NrFaKorygowanej = faktura.NrFakturyKorygowanej;
                }
                listaFaktur.Add(nowaFaktura);

                foreach (var pozycja in faktura.PozycjeFaktury)
                {
                    pozycjeFaktur.Add(new JPKFakturaWiersz()
                    {
                        P_2B = faktura.NumerFakturyCaly,
                        P_7 = pozycja.NazwaArtykulu,
                        P_8A = pozycja.JednostkaMiary,
                        P_8B = pozycja.LiczbaSztukPozycji,
                        P_8BSpecified = true,
                        P_9A = pozycja.CenaPozycjiZbytuNetto,
                        P_9ASpecified = true,
                        P_9B = pozycja.CenaPozycjiZbytuBrutto,
                        P_9BSpecified = true,
                        P_11 = pozycja.WartoscSprzedazyNetto,
                        P_11A = pozycja.WartoscSprzedazyBrutto,
                        P_11ASpecified = true,
                        P_11Specified = true,
                        P_12Specified = true,
                        P_12 = pozycja.VatEnum 
                    });
                }

            }
            var wierszeCtrl = new JPKFakturaWierszCtrl()
            {
                LiczbaWierszyFaktur = pozycjeFaktur.Count.ToString(),
                WartoscWierszyFaktur = pozycjeFaktur.Sum(p => p.P_11)
            };
            var fakturaCtrl = new JPKFakturaCtrl()
            {
                LiczbaFaktur = listaFaktur.Count.ToString(),
                WartoscFaktur = listaFaktur.Sum(s => s.P_15)
            };
            var naglowek = new JPKNaglowek()
            {
                CelZlozenia = 1,
                DataWytworzeniaJPK = DateTime.ParseExact(DateTime.Now.ToString("dd.MM.yyyy"), "dd.MM.yyyy", CultureInfo.InvariantCulture),
                //  DomyslnyKodWaluty = currCode_Type.PLN,// (currCode_Type)Enum.Parse(typeof(currCode_Type), firma.KodWalutowy),
                KodFormularza = new TNaglowekKodFormularza(),
                KodUrzedu = (TKodUS)Enum.Parse(typeof(TKodUS), "Item" + firma.UrzadSkarbowy),
                WariantFormularza = 3,
                DataOd = new DateTime(listaFaktur[0].P_1.Year, listaFaktur[0].P_1.Month, 1),
                DataDo = new DateTime(listaFaktur[0].P_1.Year, listaFaktur[0].P_1.Month, 1).AddMonths(1).AddDays(-1)
            };
            jpk.Faktura = listaFaktur;//.ToArray();
            jpk.FakturaCtrl = fakturaCtrl;
            jpk.FakturaWiersz = pozycjeFaktur;//.ToArray();
            jpk.FakturaWierszCtrl = wierszeCtrl;
            jpk.Naglowek = naglowek;
            jpk.Podmiot1 = new JPKPodmiot1()
            {
                IdentyfikatorPodmiotu = new TIdentyfikatorOsobyNiefizycznej1()
                {
                    NIP = firma.NIP,
                    PelnaNazwa = firma.PelnaNazwa
                }
                ,
                Item
                 //     AdresPodmiotu

                 = new TAdresPolski1()
                 {
                     Gmina = firma?.Gmina ?? "abcd",
                     KodKraju = TKodKraju.PL,
                     KodPocztowy = firma.KodPocztowy,
                     Miejscowosc = firma.Miejscowosc,
                     NrDomu = firma?.NrDomu,
                     NrLokalu = firma.NrLokalu,
                     Ulica = firma.Ulica,
                     Powiat = firma?.Powiat ?? "abcd",
                     Wojewodztwo = firma?.Wojewodztwo ?? "abcd"
                 }
            };
            return jpk;
        }
    }
}
