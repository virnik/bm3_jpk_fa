﻿using BM3_JPK_FA.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM3_JPK_FA.Helper
{
    public class IsolatedSettingsHelper
    {
        public static Firma PobierzDaneFirmy()
        {
            var firma = new Firma();
            IsolatedStorageFile isoStore = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null);
            var safe = new SafeAES();

            if (isoStore.FileExists("234hjgjk34h5.json"))
            {
                using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("234hjgjk34h5.json", FileMode.Open, isoStore))
                {
                    using (StreamReader reader = new StreamReader(isoStream))
                    {
                        firma = JsonConvert.DeserializeObject<Firma>(safe.Decrypt(reader.ReadToEnd())) ?? new Firma();
                    }
                }
            }
            return firma;
        }

        public static void ZapiszDaneFirmy(Firma firma)
        {
            IsolatedStorageFile isoStore = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null);
            var safe = new SafeAES();
            // var fileMode = isoStore.FileExists("firma.json") ? FileMode.Append : FileMode.

            using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("234hjgjk34h5.json", FileMode.Create, isoStore))
            {
                using (StreamWriter writer = new StreamWriter(isoStream))
                {
                    writer.Write(safe.Encrypt(JsonConvert.SerializeObject(firma)));
                }
            }
        }
    }
}
